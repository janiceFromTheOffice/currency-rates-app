import SwiftyJSON
import UIKit

struct Currency {
    let symbol: String
    let scaling: Double
    let bid10: Double
    let ask10: Double
    let bid50: Double
    let ask50: Double
    let trend: Trend
    var icon: UIImage? {
        return UIImage(named: symbol) ?? UIImage(named: "unknown")
    }
}

extension Currency {
    init?(json: JSON) {
        guard json["currency"]["active"].boolValue,
            let symbol = json["currency"]["symbol"].string,
            let scaling = json["scaling"].double,
            let bid10 = json["rates"]["10"]["bid"].double,
            let ask10 = json["rates"]["10"]["ask"].double,
            let bid50 = json["rates"]["50000"]["bid"].double,
            let ask50 = json["rates"]["50000"]["ask"].double,
            let trend = Trend(rawValue: json["trend"].stringValue)

        else {
            return nil
        }

        self = Currency(symbol: symbol, scaling: scaling, bid10: bid10, ask10: ask10, bid50: bid50, ask50: ask50, trend: trend)
    }
}

extension Currency: Comparable {
    static func < (lhs: Currency, rhs: Currency) -> Bool {
        let order = ["USD", "EUR", "GBP", "CHF"]
        let lhsOrder = order.firstIndex(of: lhs.symbol) ?? 4
        let rhsOrder = order.firstIndex(of: rhs.symbol) ?? 4
        return (lhsOrder != rhsOrder) ? lhsOrder < rhsOrder : lhs.symbol < rhs.symbol
    }
}
