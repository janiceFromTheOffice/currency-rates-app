import UIKit

final class CurrencyPresenter {
    private weak var currencyView: CurrencyView?
    private var currencyProvider: CurrencyProvider
    private(set) var currencies = [Currency]()
    private(set) var state = UnitState.tenUnits
    var itemCount: Int {
        return currencies.count
    }

    init(currencyProvider: CurrencyProvider) {
        self.currencyProvider = currencyProvider
    }

    func attachView(view: CurrencyView) {
        currencyView = view
    }

    func getCurrencies() {
        currencyProvider.fetchAPIrequest { currencies in
            self.currencies = currencies.sorted()
            self.currencyView?.refreshData()
            self.currencyView?.endRefreshing()
        }
    }

    func changeUnitState(segControl: UISegmentedControl) {
        state = segControl.selectedSegmentIndex == 0 ? UnitState.tenUnits : UnitState.fiftyUnits
        currencyView?.refreshData()
    }
}
