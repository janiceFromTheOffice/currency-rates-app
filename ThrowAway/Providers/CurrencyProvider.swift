import Alamofire
import Foundation
import SwiftyJSON

final class CurrencyProvider {
    func fetchAPIrequest(_ callBack: @escaping ([Currency]) -> Void) {
        Alamofire.request("https://mob-api.cinkciarz.pl/rates/cinkciarz?amounts=10,50000").responseJSON { (responseData) -> Void in
            if let responseValue = responseData.result.value {
                let responseJSON = JSON(responseValue)
                let currencies = responseJSON["data"].arrayValue.compactMap { Currency(json: $0) }
                callBack(currencies)
            }
        }
    }
}
