import Darwin

extension Double {
    var cleanValue: String {
        return floor(self) == self ? "\(Int(self))" : "\(self)"
    }

    var asRate: String {
        return String(format: "%.4f", self)
    }
}
