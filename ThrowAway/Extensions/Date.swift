import Foundation

extension Date {
    struct Formatter {
        static let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return formatter
        }()
    }

    var dateToString: String {
        return Formatter.dateFormatter.string(from: self)
    }
}
