import UIKit

final class CurrencyViewCell: UITableViewCell {
    @IBOutlet weak var foreignCurrencyImageView: UIImageView!
    @IBOutlet weak var localCurrencyImageView: UIImageView!
    @IBOutlet weak var foreignCurrencyLabel: UILabel!
    @IBOutlet weak var localCurrencyLabel: UILabel!
    @IBOutlet weak var scaleLabel: UILabel!
    @IBOutlet weak var buyLabel: UILabel!
    @IBOutlet weak var sellLabel: UILabel!
    @IBOutlet weak var buyArrowLabel: UILabel!
    @IBOutlet weak var sellArrowLabel: UILabel!
}
