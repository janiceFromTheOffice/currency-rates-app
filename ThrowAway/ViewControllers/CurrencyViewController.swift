import UIKit

final class CurrencyViewController: UIViewController, UITableViewDelegate {
    @IBOutlet private weak var currencyTableView: UITableView!
    @IBOutlet private weak var segControl: UISegmentedControl!
    @IBOutlet private weak var dateLabel: UILabel!

    private let refreshControl = UIRefreshControl()
    private var presenter = CurrencyPresenter(currencyProvider: CurrencyProvider())

    override func viewDidLoad() {
        super.viewDidLoad()

        addRefresh()
        presenter.attachView(view: self)
        presenter.getCurrencies()
    }

    @IBAction private func unitChange(_: Any) {
        presenter.changeUnitState(segControl: segControl)
    }

    private func updateTime() {
        let date = Date().dateToString
        dateLabel.text = date
    }

    @objc private func refreshCurrencyData(_: Any) {
        presenter.getCurrencies()
    }
}

extension CurrencyViewController: CurrencyView {
    func endRefreshing() {
        refreshControl.endRefreshing()
        updateTime()
    }

    func refreshData() {
        currencyTableView.reloadData()
    }
}

extension CurrencyViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CurrencyViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as?
            CurrencyViewCell else {
            fatalError("The dequeued cell is not an instance of CurrencyCell.")
        }

        let currentCellCurrency = presenter.currencies[indexPath.row]
        cell.foreignCurrencyImageView?.image = currentCellCurrency.icon
        cell.foreignCurrencyLabel.text = currentCellCurrency.symbol
        cell.scaleLabel.text = "\(currentCellCurrency.scaling.cleanValue) \(currentCellCurrency.symbol)"

        if presenter.state == UnitState.tenUnits {
            cell.buyLabel.text = currentCellCurrency.bid10.asRate
            cell.sellLabel.text = currentCellCurrency.ask10.asRate

        } else if presenter.state == UnitState.fiftyUnits {
            cell.buyLabel.text = currentCellCurrency.bid50.asRate
            cell.sellLabel.text = currentCellCurrency.ask50.asRate
        }

        cell.buyLabel.textColor = currentCellCurrency.trend.color
        cell.sellLabel.textColor = currentCellCurrency.trend.color
        cell.buyArrowLabel.text = currentCellCurrency.trend.arrow
        cell.sellArrowLabel.text = currentCellCurrency.trend.arrow
        cell.buyArrowLabel.textColor = currentCellCurrency.trend.color
        cell.sellArrowLabel.textColor = currentCellCurrency.trend.color

        return cell
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return presenter.itemCount
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    private func addRefresh() {
        currencyTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshCurrencyData(_:)), for: .valueChanged)
    }
}
