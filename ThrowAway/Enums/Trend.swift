import UIKit

enum Trend: String {
    case falling = "FALLING"
    case rising = "RISING"
    case standing = "STANDING"

    var arrow: String {
        switch self {
        case .falling:
            return "▼"
        case .rising:
            return "▲"
        case .standing:
            return ""
        }
    }

    var color: UIColor {
        switch self {
        case .falling:
            return #colorLiteral(red: 0.8117647059, green: 0.1764705882, blue: 0.1450980392, alpha: 1)
        case .rising:
            return #colorLiteral(red: 0.4784313725, green: 0.7450980392, blue: 0.2235294118, alpha: 1)
        case .standing:
            return #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1)
        }
    }
}
