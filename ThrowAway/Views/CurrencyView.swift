import Foundation

protocol CurrencyView: class {
    func endRefreshing()
    func refreshData()
}
