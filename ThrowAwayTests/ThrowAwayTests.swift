import Alamofire
import SwiftyJSON
import XCTest

@testable import ThrowAway

class ThrowAwayTests: XCTestCase {
    func testJSONParseAndListCount() {
        let testBundle = Bundle(for: type(of: self))
        let fileURL = testBundle.url(forResource: "testJSON", withExtension: "json")
        let activeCount = 22
        var count = 0

        do {
            let data = try Data(contentsOf: fileURL!, options: Data.ReadingOptions.alwaysMapped)
            let jsonObj = try JSON(data: data)
            let currencies = jsonObj["data"].arrayValue.compactMap { Currency(json: $0) }
            count = currencies.count

        } catch let error {
            print("parse error: \(error.localizedDescription)")
        }
        XCTAssertEqual(count, activeCount, "Number of currency objects that were parsed are incorrect.")
    }
}
