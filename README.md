# Currency Rates App by Robert Janikowski

This app displays a list of currency rates through RESTful API's.  
Architecture pattern is Model View Presenter.

# To install(run in terminal): 

$ sudo gem install cocoapods  
pod install

